init();


function init() {
	$mainContainer = $("#main-container");
}


function Navigation(context) {

	if (typeof context !== "string") {
		console.log("lol" + context);
		var pageName = context.textContent.toLowerCase();


	}
	else {

		pageName = context;
	}
	var htmlSuffix = '.html'
	$mainContainer.load(pageName + htmlSuffix, function(responseText, textStatus, req) {
		if (textStatus == "error") {
			return "Unable to find the page";
		}
		var content = document.getElementById(pageName).content;
		document.querySelector('#main-container').appendChild(
			content.cloneNode(true));

		if (pageName === "reports") {
			reportsModule();
		}
		if (pageName === "timer") {
			timerModule();
		}
	});
}


function reportsModule() {
	console.log("graph page loaded");
	reportsLoader();
	reportsHandler.test();
}

function timerModule() {
	//var timerHandler = new timerHandler();
	console.log("timer page loaded");
	$(".manuallyButton").click(function() {
		$('.timerRowManual').toggle();
		$('.timerRow').toggle()
		timerHandler.buttonToggle(this);
		
	});
}
var reportsHandler = {
		test: function test() {
	      $('input[name="daterange"]').daterangepicker();
	     }
}

var timerHandler = {
	buttonToggle: function(context) {
		console.log("clicked " + context)
		$(context).text(function(i, text) {
			return text === "Add manually" ? "Use timer" : "Add manually";
		})

    

	}


}


function reportsLoader() {
	var dData = function() {
		return Math.round(Math.random() * 90) + 10
	};

	var barChartData = {
		labels: ["dD 1", "dD 2", "dD 3", "dD 4", "dD 5", "dD 6", "dD 7", "dD 8", "dD 9", "dD 10"],
		datasets: [{
			fillColor: "rgba(0,60,100,1)",
			strokeColor: "black",
			data: [dData(), dData(), dData(), dData(), dData(), dData(), dData(), dData(), dData(), dData()]
		}]
	}

	var index = 11;
	var ctx = document.getElementById("canvas").getContext("2d");
	var mainChart = new Chart(ctx).Bar(barChartData, {
		responsive: true,
		barValueSpacing: 2
	});
	var canvas = document.querySelector('canvas');


	setInterval(function() {
		mainChart.removeData();
		mainChart.addData([dData()], "dD " + index);
		index++;
	}, 3000);



	var data = [{
			value: 30,
			color: "#0BE8E8"
		}, {
			value: 50,
			color: "#1FBFF0"
		}, {
			value: 100,
			color: "#F04A18"
		}, {
			value: 40,
			color: "#F2AE24"
		}, {
			value: 120,
			color: "#4D5360"
		}

	]

	var options = {
		animation: false,
		responsive: true,
		maintainAspectRatio: true
	};

	//Get the context of the canvas element we want to select
	var c = $('#donutchart');
	var ct = c.get(0).getContext('2d');
	var ctx = document.getElementById("donutchart").getContext("2d");
	/*************************************************************************/
	myNewChart = new Chart(ct).Doughnut(data, options);

}